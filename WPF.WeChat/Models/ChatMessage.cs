﻿namespace WPF.WeChat.Models
{
    public class ChatMessage
    {
        public string Message { get; set; }
        public bool IsSend { get; set; }
    }
}
