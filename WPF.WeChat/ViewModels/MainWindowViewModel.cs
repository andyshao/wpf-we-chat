﻿

using System.Collections.ObjectModel;
using System.Windows.Input;
using WPF.WeChat.Common;
using WPF.WeChat.Models;

namespace WPF.WeChat.ViewModels
{
    public class Friend
    {
        public string IconPath { get; set; }
        public string Name { get; set; }
    }

    public class MainWindowViewModel : ObservableObject
    {
        private ObservableCollection<Friend> _contactList = new ObservableCollection<Friend>(){
            new Friend(){Name="机器人1号"},
            new Friend(){Name="机器人2号"},
            new Friend(){Name="机器人3号"},
            new Friend(){Name="机器人4号"},
            new Friend(){Name="机器人5号"},
            new Friend(){Name="机器人6号"}
        };
        private ObservableCollection<ChatMessage> m_Messages = new ObservableCollection<ChatMessage>();
        public ObservableCollection<ChatMessage> Messages { get { return m_Messages; } }

        public ObservableCollection<Friend> ContactList
        {
            get { return _contactList; }
            set { this.SetProperty(ref _contactList, value); }
        }


        public MainWindowViewModel()
        {
            Messages.Add(new ChatMessage() { IsSend = false, Message = "恭喜！你被录用了" });
            Messages.Add(new ChatMessage() { IsSend = false, Message = "明天来总部上班" });
            Messages.Add(new ChatMessage() { IsSend = true, Message = "OK" });
            Messages.Add(new ChatMessage() { IsSend = false, Message = "给你发1000万年薪" });
            Messages.Add(new ChatMessage() { IsSend = false, Message = "小伙子加油干" });
            Messages.Add(new ChatMessage() { IsSend = true, Message = "OK" });
        }

        public int SelectedMessage { get; set; }
        private string _message;

        public string Message
        {
            get { return _message; }
            set { _message = value; RaisePropertyChanged();}
        }


        public bool CanSendMessage
        {
            get { return !string.IsNullOrEmpty(Message); }
        }
        public ICommand SendMessageCommand
        {
            get => _sendMessageCommand ?? (_sendMessageCommand = new RelayCommand(SendMessage));
            set => _sendMessageCommand = value;
        }
        private ICommand _sendMessageCommand;
        public void SendMessage()
        {
            m_Messages.Add(new ChatMessage() { IsSend = true, Message = Message });
           // m_Messages.Add(new ChatMessage() { IsSend = true, Message = "好！" });

            SelectedMessage = m_Messages.Count - 1;
           Message="";
        }
    }
}
