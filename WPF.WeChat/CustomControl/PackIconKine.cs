﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WPF.WeChat.CustomControl
{
    public enum PackIconKind
    {
        guding,
        hezi,
        biaoqing1,
        Add,
        jianqie,
        sousuo,
        look,
        shipindianhua,
        xiaoxi,
        Close,
        normalsize,
        min,
        shoucang,
        tongxunlu,
        folderline,
        moment,
        xiaochengxu,
        gengduo,
        biaoqiankuozhan_liaotian,
        shouji,
        quan
    }

}
