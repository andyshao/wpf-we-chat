﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using WPF.WeChat.ViewModels;

namespace WPF.WeChat
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            var view=new MainWindow();
            var vm=new MainWindowViewModel();
            view.DataContext=vm;
            view.ShowDialog();
        }
    }
}
